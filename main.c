#include <stdio.h>
#include <stdlib.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>

int
main()
{
    Display *dp;
    int s;
    dp = XOpenDisplay(NULL);
    if (dp == NULL)
    {
        printf("Can't open display\n");
        exit(0);
    }
    printf("Display opened\n");
    s = DefaultScreen(dp);
    Window w = XCreateSimpleWindow(dp, RootWindow(dp,s), 200, 200, 200,200,0,0,0);
    XDestroyWindow(dp, w);
    XCloseDisplay(dp);
    printf("Display closed\n");
	return 0;
}
